import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp2d

omega = 0 #omega = omega g
alpha = 0.80 #gab = alpha g

mu = 1
T = 0

####stato iniziale####

###operatori b1
def omegab1(k):
    return np.abs(k)*np.sqrt(k**2+2)

def sumb1(k):
    return (k**2/omegab1(k))**(0.5)
def diffb1(k):
    return 1/(k**2/omegab1(k))**(0.5)

def s(k):
    return 1/2*(sumb1(k)+diffb1(k))
def t(k):
    return 1/2*(sumb1(k)-diffb1(k))

def nb1(k):
    if T>0:
        return 1/(np.exp(omegab1(k)/T)-1)
    else:
        return 0

###operatori b2
def omegab2(k):
    return k**2 +alpha-mu
def nb2(k):
    if T>0:
        return 1/(np.exp(omegab2(k)/T)-1)
    else:
        return 0

####operatori 1####
def omega1(k):
    return np.abs(k)*np.sqrt(k**2+(1+alpha))

def sum1(k):
    return 1/np.sqrt(2)*(k**2/omega1(k))**(0.5)
def diff1(k):
    return 1/np.sqrt(2)*1/(k**2/omega1(k))**(0.5)

def u1(k):
    return 1/2*(sum1(k)+diff1(k))
def v1(k):
    return 1/2*(sum1(k)-diff1(k))

def alpha1(k):
    return np.sqrt(2)*(u1(k)*s(k)-v1(k)*t(k))
def beta1(k):
    return -np.sqrt(2)*(v1(k)*s(k)-u1(k)*t(k))

def n1(k):
    return beta1(k)**2 + (alpha1(k)**2+beta1(k)**2)*nb1(k)
def a1(k, t):
    return alpha1(k)*beta1(k)*(1+nb1(k))*np.exp(-2*1j*omega1(k)*t)

####operatori 2####
def omega2(k):
    return np.sqrt((k**2)*(k**2+1-alpha+4*omega)+2*omega*(1-alpha+2*omega))

def sum2(k):
    return 1/np.sqrt(2)*((k**2+2*omega)/omega2(k))**(0.5)
def diff2(k):
    return 1/np.sqrt(2)*1/((k**2+2*omega)/omega2(k))**(0.5)

def u2(k):
    return 1/2*(sum2(k)+diff2(k))
def v2(k):
    return 1/2*(sum2(k)-diff2(k))

def alpha2(k):
    return np.sqrt(2)*u2(k)
def beta2(k):
    return -np.sqrt(2)*v2(k)

def n2(k):
    return beta2(k)**2 + (alpha2(k)**2+beta2(k)**2)*nb2(k)
def a2(k, t):
    return alpha2(k)*beta2(k)*(1+nb2(k))*np.exp(-2*1j*omega2(k)*t)

f = 1000
mult = 10

###plot dispersioni1
ls = np.linspace(-2, 2, 1000)
plt.plot(ls, omegab1(ls), label=r'$\omega^1_k$')
plt.plot(ls, omegab2(ls), label=r'$\omega^2_k$')
plt.vlines([np.sqrt(2), -np.sqrt(2)], omega1(ls).min(), omega1(ls).max(), colors=['red', 'red'], linestyles='dashed', label=r'$\sqrt{2}/\xi$')
plt.legend()
plt.xlabel(r"$k\xi$")
plt.ylabel(r"$\omega_k \tau$")
plt.savefig('dispersioni1.pdf')
plt.close()

###plot dispersioni2
ls = np.linspace(-2, 2, 1000)
fig, axs = plt.subplots(2)

#axs[0].set_title("$\Omega = $"+str(omega)+"$gn, g_{ab} = $" + str(alpha) + "$g$")
axs[0].plot(ls, omega1(ls))
axs[0].vlines([np.sqrt(1+alpha), -np.sqrt(1+alpha)], omega1(ls).min(), omega1(ls).max(), colors=['red', 'red'], linestyles='dashed', label=r'$1/\xi_d$')
axs[0].legend()

#axs[0].legend([r"$\xi^1_k$"])
axs[0].set_xlabel(r"$k\xi$")
axs[0].set_ylabel(r"$\omega^1_k \tau$")

ls = np.linspace(-0.8, 0.8, 1000)
axs[1].plot(ls, omega2(ls))

#axs[1].legend([r"$\xi^2_k$"])
axs[1].set_xlabel(r"$k\xi$")
axs[1].set_ylabel(r"$\omega^2_k \tau$")
axs[1].vlines([np.sqrt(1-alpha), -np.sqrt(1-alpha)], omega1(ls).min(), omega1(ls).max(), colors=['red', 'red'], linestyles='dashed', label=r'$1/\xi_s$')
axs[1].legend()


plt.tight_layout()
plt.savefig('dispersioni2.pdf')
plt.close()

###plot coefficienti

fig, axs = plt.subplots(2)
axs[0].plot(ls, u1(ls))
axs[0].plot(ls, v1(ls))
axs[0].set_title("Per $\omega_1$")
axs[0].legend(["$u^1$", "$v^1$"])
axs[0].set_ylim((-4, 4))

axs[1].plot(ls, u2(ls))
axs[1].plot(ls, v2(ls))
axs[1].set_title("Per $\omega_2$")
axs[1].legend(["$u^2$", "$v^2$"])
axs[1].set_ylim((-4, 4))

plt.savefig('coefficienti.pdf')
plt.close()


ls = np.linspace(0, 10, 1000)

fig, axs = plt.subplots(2)
axs[0].plot(ls, (u1(ls)+v1(ls))**2)
axs[0].plot(ls, (u2(ls)+v2(ls))**2)
axs[0].legend(["$(u^1+v^1)^2$", "$(u^2+v^2)^2$"])
axs[0].set_xlabel(r"$k\xi$")
axs[1].plot(ls, (u1(ls)+v1(ls))**2*(n1(ls)+a1(ls, 0)))
axs[1].plot(ls, (u2(ls)+v2(ls))**2*(n2(ls)+a2(ls, 0)))
axs[1].legend([r"$(u^1+v^1)^2\times(n^1+a^1)$", r"$(u^2+v^2)^2\times(n^2+a^2)$"])
axs[1].set_xlabel(r"$k\xi$")

plt.tight_layout()
plt.savefig('termini.pdf')
plt.close()


###plot n1 e n2 in funzione della T

ls = np.linspace(-3, 3, 1000)

fig, axs = plt.subplots(2)
plt.title('T = '+str(T))

axs[0].set_title(r'$\langle c_k^{1\dagger} c^1_k\rangle$')
T = 0
axs[0].plot(ls, n1(ls))
T = 0.5
axs[0].plot(ls, n1(ls))
#the c1 are thermally populated
axs[0].plot(ls, 1/(np.exp(omega1(ls))-1), '--')
axs[0].legend(["$T=0$", "$T=0.5$", "T=0.5 ad"])
axs[0].set_xlabel(r"$k\xi$")
axs[0].set_yscale('log')

axs[1].set_title(r'$\langle c_k^{2\dagger} c^2_k\rangle$')
T = 0
axs[1].plot(ls, n2(ls))
T = 0.5
axs[1].plot(ls, n2(ls))
#the c2 are thermally populated
axs[1].plot(ls, 1/(np.exp(omega2(ls))-1), '--')
axs[1].legend(["$T=0$", "$T=0.5$", "T=0.5 ad"])
axs[1].set_xlabel(r"$k\xi$")
axs[1].set_yscale('log')

plt.tight_layout()
plt.savefig('popolazioni.pdf')
plt.close()



###plot anomalous in funzione della T

ls = np.linspace(-1, 1, 1000)

fig, axs = plt.subplots(2)
axs[0].set_title(r'$\langle c_k^1 c^1_{-k}\rangle$')
T = 0
axs[0].plot(ls, np.abs(a1(ls, 0)))
T = 0.5
axs[0].plot(ls, np.abs(a1(ls, 0)))
axs[0].legend(["$T=0$", "$T=0.5$"])
axs[0].set_xlabel(r"$k\xi$")
axs[0].set_yscale('log')

axs[1].set_title(r'$\langle c_k^2 c^2_{-k}\rangle$')
T = 0
axs[1].plot(ls, a2(ls, 0))
T = 0.5
axs[1].plot(ls, a2(ls, 0))
axs[1].legend(["$T=0$", "$T=0.5$"])
axs[1].set_xlabel(r"$k\xi$")
axs[1].set_yscale('log')

plt.tight_layout()
plt.savefig('anomalous.pdf')
plt.close()


T = 0
###magnetizzazione###

def tmag(k, delta, t):
    sum = 0
    sum += np.exp(1j*k*delta)*(u2(k)+v2(k))**2*(a2(k, t)+n2(k))
    sum += np.conjugate(sum)
    sum += np.exp(1j*k*delta)*((u2(k)+v2(k))**2-1/2)
    return sum

def gmag(delta, t):
    sum = 0
    for i in range(-mult*f, mult*f):
        k = 2*np.pi*(i/f)
        if k != 0:
            sum += tmag(k, delta, t)
    return 2*sum/f


ls = np.linspace(0, 100, 1000)
plt.ylim((-0.05, 0.05))
plt.plot(ls, gmag(ls, 0))
plt.plot(ls, gmag(ls, 20))
plt.plot(ls, gmag(ls, 40))
plt.plot(ls, gmag(ls, 60))
plt.legend([0, 20, 40, 60], title=r"$t/\tau$")

plt.xlabel(r"$|x-x'|/\xi$")
plt.ylabel(r"$(n\xi)\times \delta m^{(2)}(x,x')$")

plt.title('T = '+str(T))

plt.tight_layout()
plt.savefig('grafico_mag.pdf')
plt.close()


###densità###

def tden(k, delta, t):
    sum = 0
    sum += np.exp(1j*k*delta)*(u1(k)+v1(k))**2*(a1(k, t)+n1(k))
    sum += np.conjugate(sum)
    sum += np.exp(1j*k*delta)*((u1(k)+v1(k))**2-1/2)
    return sum

def gden(delta, t):
    sum = 0
    for i in range(-mult*f, mult*f):
        k = 2*np.pi*(i/f)
        if k != 0:
            sum += tden(k, delta, t)
    return 2*sum/f

ls = np.linspace(0, 100, 1000)
plt.ylim((-0.01, 0.01))
plt.plot(ls, gden(ls, 0))
plt.plot(ls, gden(ls, 5))
plt.plot(ls, gden(ls, 10))
plt.plot(ls, gden(ls, 15))
plt.legend([0, 5, 10, 15], title=r"$t/\tau$")

plt.xlabel(r"$|x-x'|/\xi$")
plt.ylabel(r"$(n\xi)\times \delta n^{(2)}(x,x')$")

plt.title('T = '+str(T))

plt.tight_layout()
plt.savefig('grafico_den.pdf')
plt.close()

#meshhhhhhhh
#size_x = 1000
#size_y =  60
#x = np.linspace(0, 100, size_x) 
#y = np.linspace(0, 60, size_y)
#z = np.array([np.real(gmag(x, t)) for t in y])
#
#print(z)
#
#X, Y = np.meshgrid(x, y)
#
#pc = plt.pcolormesh(X, Y, z, cmap='plasma', vmin=-0.04, vmax=0.04, shading='gouraud')
#
#plt.colorbar(pc)
#plt.ylabel(r"$t/\tau$")
#plt.xlabel(r"$|x-x'|/\xi$")
#plt.savefig('plasma.pdf')
#plt.close()