\documentclass{beamer}
\usepackage{subfig}
\usepackage{braket}
\usepackage{graphicx, array}
\renewcommand{\vec}[1]{\mathbf{#1}}
\setbeamertemplate{section in toc}[sections numbered]

\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

\usepackage{yfonts}
\title{Dynamical Casimir emission in an atomic BEC: a theoretical study in view of the experiment}
\subtitle{Colloqui della Classe di Scienze}
\author{Francesco Debortoli \\ Prof. Iacopo Carusotto}
\date{Spring 2023}

\begin{document}

\frame{\titlepage}

\begin{frame}
\frametitle{Table of Contents}
\tableofcontents
\end{frame}

\begin{frame}
    \section{Motivations}
    \frametitle{Motivations}
    {\Large
    \begin{align*}
        H_1 &\xrightarrow[]{\text{quench}} H_2\\
        \ket{\text{GS1}} &\xrightarrow[]{\text{quench}} \ket{\text{GS1}} \neq \ket{\text{GS2}}
    \end{align*}}
    {\Large
    \begin{gather*}
        \ket{\Psi(t=0^+)} = \ket{\text{GS1}}
    \end{gather*}}
    \begin{gather*}
        \overset{\mathrm{C.C.}}{=} \sum_{\vec{k}} \text{pairs of quasiparticles with momenta } \hbar\vec{k}, -\hbar\vec{k}
    \end{gather*}
    {\Large
    \begin{align*}
        \text{Goal: study the evolution }\ket{\Psi(t>0)} &\text{ given by } H_2
    \end{align*}
    \begin{align*}
        g^{(2)}(x, x', t)=\braket{\hat{O}(x, t)\hat{O}(x', t)}
    \end{align*}}
\end{frame}


\begin{frame}
    \section{Why a Bose-Einstein condensate? Why a spinorial one?}
    \frametitle{Why a Bose-Einstein condensate?}
    \begin{itemize}
        \item Low $T$ $\longrightarrow$ quantum effects!
        \item Microscopic model
        \begin{align*}
        H &= \int d\vec{r} \Psi^\dagger(\vec{r})h_1\Psi(\vec{r})+\frac{g}{2}\int d\vec{r} \Psi^\dagger(\vec{r})\Psi^\dagger(\vec{r})\Psi(\vec{r})\Psi(\vec{r})\\
        g &= \frac{4\pi \hbar^2}{m}a
        \end{align*}
        \item Weakly interacting $\longrightarrow$ analytical many-body theory avaible!
        \begin{align*}
            na^3 \ll 1
        \end{align*}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Why a spinorial one?}
    \begin{align*}
        H = \int d\vec{r} \Psi_a^\dagger(\vec{r})h_1^a\Psi_a(\vec{r})+\frac{g_{aa}}{2}\int d\vec{r} \Psi_a^\dagger(\vec{r})\Psi_a^\dagger(\vec{r})\Psi_a(\vec{r})\Psi_a(\vec{r}) \\
        + \int d\vec{r} \Psi_b^\dagger(\vec{r})h_1^b\Psi_b(\vec{r})+\frac{g_{bb}}{2}\int d\vec{r} \Psi_b^\dagger(\vec{r})\Psi_b^\dagger(\vec{r})\Psi_b(\vec{r})\Psi_b(\vec{r})\\
        +g_{ab}\int d\vec{r}\Psi_a^\dagger(\vec{r})\Psi_b^\dagger(\vec{r})\Psi_b(\vec{r})\Psi_a(\vec{r})\\
        + \Omega\int d\vec{r} \Psi_a^\dagger(\vec{r})\Psi_b(\vec{r}) + \Omega^*\int d\vec{r} \Psi_a(\vec{r})\Psi_b^\dagger(\vec{r})
    \end{align*}
    \begin{itemize}
        \item Spinorial order parameter
        \begin{align*}
            \begin{bmatrix}
                \sqrt{n_a}e^{i\theta_a}\\
                \sqrt{n_b}e^{i\theta_b}
            \end{bmatrix}
        \end{align*}
        \item Relative phase $\theta_{ba}$ becomes an observable
    \end{itemize}
\end{frame}

\begin{frame}
    \section{Bogolioubov theory of diluite Bose gas}
    \frametitle{GP equations and Bogolioubov Hamiltonian}
    \begin{itemize}
        \item Field operator = classical part + quantum fluctuactions
    {\footnotesize
    \begin{align*}
        \begin{bmatrix}
            \Psi_a(\vec{r}, t) \\
            \Psi_b(\vec{r}, t)
        \end{bmatrix}=
        \begin{bmatrix}
            \psi_a(\vec{r}, t) \\
            \psi_b(\vec{r}, t)
        \end{bmatrix}
        +\sum_{\vec{k}\neq 0}\begin{bmatrix}
            \star p^1_\vec{k}(t) + \star p^{1\dagger}_{-\vec{k}}(t) +\star p^2_\vec{k}(t) +\star p^{2\dagger}_{-\vec{k}}(t)\\
            \star p^1_\vec{k}(t) +\star p^{1\dagger}_{-\vec{k}}(t) +\star p^2_\vec{k}(t) +\star p^{2\dagger}_{-\vec{k}}(t)
        \end{bmatrix} \frac{e^{i\vec{k}\cdot\vec{r}}}{\sqrt{V}}
    \end{align*}}
        \item GPE $\longrightarrow$ evolution of the classical part
    \begin{align*}
        i\hbar \frac{\partial}{\partial t} \psi_a &= \left[h_1^a + g_a|\psi_a|^2+g_{ab}|\psi_b|^2\right]\psi_a + \Omega \psi_b\\
        i\hbar \frac{\partial}{\partial t} \psi_b &= \left[h_1^b + g_b|\psi_b|^2+g_{ab}|\psi_a|^2\right]\psi_b + \Omega^* \psi_a
    \end{align*}
        \item Bogolioubov Hamiltonian $\longrightarrow$ quantum fluctuactions
    \begin{align*}
        H = \sum_{\vec{k}\neq 0} \hbar\omega_k^1p^{1\dagger}_\vec{k}p^1_\vec{k} + \hbar\omega_k^2p^{2\dagger}_\vec{k}p^2_\vec{k} + \text{GS energy}
    \end{align*}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{$H_1$: completely polarized ($n_a = n, n_b = 0$)}
    \section{$H_1$ and $H_2$ in our case? Polarized and neutral GSs}
\begin{align*}
    \begin{bmatrix}
        \Psi_a(\vec{r}, t)\\
        \Psi_b(\vec{r}, t)
    \end{bmatrix}
    = e^{-i\mu t/\hbar}
    \begin{bmatrix}
        \sqrt{n}\\
        0
    \end{bmatrix}
    +\sum_{\vec{k}\neq 0}\begin{bmatrix}
        s_k b^1_\vec{k}(t) + t_k b^{1\dagger}_\vec{-k}(t)\\
        b^2_\vec{k}(t)
    \end{bmatrix}
    \frac{e^{i\vec{k}\cdot\vec{r}}}{\sqrt{V}}
\end{align*}
    \begin{tabular}{C{8cm}  L{2cm}}
        \includegraphics[width=\linewidth]{figures/dispersioni1.pdf} & 
        \begin{gather*}
        \frac{\hbar^2}{2m\xi^2}=gn\\
        v \approx \sqrt{\frac{ng}{m}}
        \end{gather*} 
    \end{tabular}
\end{frame}

\begin{frame}
    \frametitle{$H_2$: neutral ground state ($n_a = n/2, n_b = n/2$)}
{\scriptsize
\begin{align*}
    \begin{bmatrix}
        \Psi_a(\vec{r}, t)\\
        \Psi_b(\vec{r}, t)
    \end{bmatrix}
    = e^{-i\mu t/\hbar}
    \begin{bmatrix}
        \sqrt{n/2}\\
        -\sqrt{n/2}
    \end{bmatrix}
    +
    \sum_{\vec{k}\neq 0}
    \begin{bmatrix}
        \hphantom{-}u^1_k c^1_{\vec{k}}(t) + v^1_k c^{1\dagger}_{\vec{-k}}(t) + u^2_k c^2_\vec{k}(t) + v^2_k c^{2\dagger}_\vec{-k}(t)\\
        -u^1_k c^1_{\vec{k}}(t) - v^1_k c^{1\dagger}_{\vec{-k}}(t) + u^2_k c^2_\vec{k}(t) + v^2_k c^{2\dagger}_\vec{-k}(t)
    \end{bmatrix}
    \frac{e^{i\vec{k}\cdot\vec{r}}}{\sqrt{V}}
\end{align*}}

    \begin{tabular}{C{8cm}  L{2cm}}
        \includegraphics[width=\linewidth]{figures/dispersioni2.pdf} & 
        {\footnotesize
        \begin{gather*}
        \frac{\hbar^2}{2m\xi_d^2}=(g+g_{ab})n\\
        v_d \approx \sqrt{\frac{n(g+g_{ab})}{2m}}\\[8ex]
        \frac{\hbar^2}{2m\xi_s^2}=(g-g_{ab})n\\
        v_s \approx \sqrt{\frac{n(g-g_{ab})}{2m}}\\[8ex]
        \end{gather*}}
    \end{tabular}
\end{frame}

\begin{frame}
    \frametitle{The quench on the Bloch sphere}
    \section{The quench on the Bloch sphere}
    \begin{figure}
        \includegraphics[width=\textwidth]{figures/bloch.png}
    \end{figure}
\begin{align*}
    c^1_\vec{k}(0^+)&=\sqrt{2}(u^1_ks_k-v^1_kt_k)b^1_\vec{k}(0^-)-\sqrt{2}(v^1_ks_k-u^1_kt_k)b^{1\dagger}_{-\vec{k}}(0^-)\\
    c^2_\vec{k}(0^+)&=\sqrt{2}u^2_kb^2_\vec{k}(0^-)-\sqrt{2}v^2_kb^{2\dagger}_{-\vec{k}}(0^-)
\end{align*}
\end{frame}

\begin{frame}
    \section{Observables? Density and magnetization correlations}
    \frametitle{Observables? Density and magnetization correlations}
{\Large
\begin{align*}
    n(x) &\equiv \Psi_a^\dagger(x)\Psi_a(x)+\Psi_b^\dagger(x)\Psi_b(x) \longrightarrow \braket{n(x)n(x')}\\
    m(x) &\equiv \Psi_a^\dagger(x)\Psi_a(x)-\Psi_b^\dagger(x)\Psi_b(x) \longrightarrow \braket{m(x)m(x')}
\end{align*}}
\end{frame}

\begin{frame}
    \begin{figure}
        \includegraphics[width=6cm]{figures/grafico_den.pdf}
    \end{figure}
    \begin{figure}
        \includegraphics[width=6cm]{figures/grafico_mag.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Why are magnetization oscillations much bigger?}
    \begin{figure}
        \includegraphics[width=\textwidth]{figures/rotation.png}
    \end{figure}
\end{frame}


\begin{frame}
    \frametitle{From the theory to prof. Ferrari's lab}
    \section{From the theory to prof. Ferrari's lab}

    \begin{tabular}{C{5cm}  L{4cm}}
        \includegraphics[width=\linewidth]{figures/raman.png} &
\begin{align*}
\dot{\vec{s}}(\vec{r}) =
\begin{bmatrix}
    \Omega\\0\\\delta+ \frac{(g-g_{ab})}{\hbar} s_z(\vec{r})
\end{bmatrix}\times \vec{s}(\vec{r})
\end{align*}
    \end{tabular}
    \begin{center}
    Selective imaging and relative phase measurement possibile!
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{A clear signature}
    \begin{figure}
        \includegraphics[width=\textwidth]{figures/plasma.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Thank you!}
    \begin{itemize}
        \item Further reading (more theory, calculation's details, finite $T$ effects, references) $\longrightarrow$ \url{https://gitlab.com/fradeb/bec/-/blob/main/appunti/main.pdf}
        \item These slides $\longrightarrow$ \url{https://gitlab.com/fradeb/bec/-/blob/main/presentazione/main.pdf}
        \item The codes $\longrightarrow$ \url{https://gitlab.com/fradeb/bec/-/tree/main/codici}
    \end{itemize}
    \vspace{1.5cm}
    \begin{center}
    Special thanks to prof. Gabriele Ferrari and prof. Iacopo Carusotto!
    \end{center}
\end{frame}
\end{document}
